import sequence_extractor

DSSP_CLASSIFICATION = ['G', 'H', 'I', 'T', 'E', 'B', 'S']


def evaluate(pdb_id, peptide_size_base, peptide_size_min, peptide_size_max, overlap_base, overlap_max,
             important_secondary_structure_signs=DSSP_CLASSIFICATION, secondary_structure_sensitivity=False):
    peptide_per_chain = {}
    sequences = sequence_extractor.get_sequence(pdb_id)
    for chain_key in sequences.keys():
        structure = sequences[chain_key]
        primary_structure = [x[0] for x in structure]
        secondary_structure = [x[1] for x in structure]
        peptide_per_chain[chain_key] = []
        peptides = evaluate_chain(secondary_structure, peptide_size_base, peptide_size_min, peptide_size_max,
                                  overlap_base, overlap_max, important_secondary_structure_signs,
                                  secondary_structure_sensitivity)
        peptide_per_chain[chain_key].append((0, (primary_structure, secondary_structure)))
        for peptide in peptides:
            sub_primary_structure = primary_structure[peptide[0]: peptide[1] + 1]
            sub_secondary_structure = secondary_structure[peptide[0]: peptide[1] + 1]
            peptide_per_chain[chain_key].append((peptide[0], (sub_primary_structure, sub_secondary_structure)))
    return peptide_per_chain


PRIMARY_SCORE = 0
SECONDARY_SCORE = 1
PEPTIDE_SIZE = 2
LINK = 3


def evaluate_chain(sseq, peptide_size_base, peptide_size_min, peptide_size_max, overlap_base, overlap_max,
                   secondary_structure_letters, secondary_structure_sensitivity):
    chain = calculate_number_chain(sseq, secondary_structure_letters, secondary_structure_sensitivity)
    scoreboard = calculate_scoreboard(chain, peptide_size_base, peptide_size_min, peptide_size_max, overlap_base,
                                      overlap_max)
    peptide_chain = calculate_peptide_chain(scoreboard)
    return create_peptides(scoreboard, peptide_chain)


def calculate_number_chain(sseq, secondary_structure_letters, secondary_structure_sensitivity):
    chain = []
    for c in sseq:
        if (c not in secondary_structure_letters) or (c not in DSSP_CLASSIFICATION):
            chain.append(0)
        else:
            if secondary_structure_sensitivity:
                chain.append(DSSP_CLASSIFICATION.index(c) + 1)
            else:
                chain.append(1)
    return chain


def calculate_scoreboard(chain, peptide_size_base, peptide_size_min, peptide_size_max, overlap_base, overlap_max):
    scoreboard = initialize_scoreboard()
    total_size = len(chain)
    initialize_scores(scoreboard, total_size, peptide_size_base)
    sizes = get_possible_sizes(peptide_size_base, peptide_size_min, peptide_size_max)
    overlaps = get_possible_overlaps(overlap_base, overlap_max)
    for i in reversed(range(0, total_size - peptide_size_base)):
        calculate_column(scoreboard, i, chain, sizes, overlaps, peptide_size_base, overlap_base)
    return scoreboard


def initialize_scoreboard():
    scoreboard = {}
    for i in range(0, 4):
        scoreboard[i] = {}
    return scoreboard


def initialize_scores(scoreboard, total_size, peptide_size):
    for i in range(1, peptide_size + 1):
        scoreboard[PRIMARY_SCORE][total_size - i] = 0
        scoreboard[SECONDARY_SCORE][total_size - i] = peptide_size - i
        scoreboard[PEPTIDE_SIZE][total_size - i] = i
        scoreboard[LINK][total_size - i] = -1


def get_possible_sizes(peptide_size_base, peptide_size_min, peptide_size_max):
    return range((min(peptide_size_base, peptide_size_min)), max(peptide_size_base, peptide_size_max) + 1)


def get_possible_overlaps(overlap_base, overlap_max):
    return range(overlap_base, max(overlap_base, overlap_max) + 1)


def calculate_column(scoreboard, i, chain, sizes, overlaps, peptide_size, peptide_overlap):
    total_size = len(chain)
    scoreboard[PRIMARY_SCORE][i] = total_size
    scoreboard[SECONDARY_SCORE][i] = total_size
    for size in sizes:
        for overlap in overlaps:
            next_peptide = calculate_next(i, size, overlap, total_size)
            if next_peptide < -1:
                continue
            if next_peptide == -1:
                primary_score = 0
            else:
                primary_score = calculate_primary_score(chain, i, next_peptide, scoreboard, size)
            if primary_score > scoreboard[PRIMARY_SCORE][i]:
                continue
            secondary_score = calculate_secondary_score(size, overlap, peptide_size, peptide_overlap)
            if (primary_score < scoreboard[PRIMARY_SCORE][i]) or (secondary_score < scoreboard[SECONDARY_SCORE][i]):
                replace_best_peptide(i, next_peptide, primary_score, scoreboard, secondary_score, size)


def replace_best_peptide(column, next_peptide, primary_score, scoreboard, secondary_score, size):
    scoreboard[PRIMARY_SCORE][column] = primary_score
    scoreboard[SECONDARY_SCORE][column] = secondary_score
    scoreboard[PEPTIDE_SIZE][column] = size
    scoreboard[LINK][column] = next_peptide


def calculate_primary_score(chain, column, next_peptide, scoreboard, size):
    primary_score = scoreboard[PRIMARY_SCORE][next_peptide]
    if is_crossing_peptide(chain, column, size, next_peptide):
        primary_score += 1
    return primary_score


def calculate_secondary_score(size, overlap, peptide_size, peptide_overlap):
    return abs(peptide_size - size) + abs(peptide_overlap - overlap)


def calculate_next(column, size, overlap, number_of_columns):
    if column + size > number_of_columns:
        return -2
    if overlap >= size:
        return -2
    if column + size == number_of_columns:
        return -1
    return column + size - overlap


def is_crossing_peptide(chain, column, size, next_column):
    last_in = chain[column + size - 1]
    first_out = chain[column + size]
    if (last_in > 0) and (same_meaning(last_in, first_out)):
        sequence_start = calculate_secondary_sequence_start(chain, column, last_in)
        if next_column <= sequence_start:
            return False
        return True
    else:
        return False


def same_meaning(first, second):
    return first == second


def calculate_secondary_sequence_start(chain, column, letter):
    while chain[column] == letter:
        column -= 1
    return column + 1


def calculate_peptide_chain(scoreboard):
    peptide_chain = []
    next = 0
    while next >= 0:
        peptide_chain.append(next)
        next = scoreboard[LINK][next]
    return peptide_chain


def create_peptides(scoreboard, peptide_chain):
    peptides = []
    for peptide in peptide_chain:
        peptides.append((peptide, peptide + scoreboard[PEPTIDE_SIZE][peptide] - 1))
    return peptides
