# coding=utf-8
import os
import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from peptid_calculator import evaluate
from win32api import GetSystemMetrics

# a subclass of Canvas for dealing with resizing of windows
class ResizingCanvas(Canvas):
    def __init__(self,parent,**kwargs):
        Canvas.__init__(self,parent,**kwargs)
        self.bind("<Configure>", self.on_resize)
        self.height = self.winfo_reqheight()
        self.width = self.winfo_reqwidth()

    def on_resize(self,event):
        # determine the ratio of old width/height to new width/height
        wscale = float(event.width)/self.width
        hscale = float(event.height)/self.height
        self.width = event.width
        self.height = event.height
        # resize the canvas
        self.config(width=self.width, height=self.height)
        # rescale all the objects tagged with the "all" tag
        self.scale("all",0,0,wscale,hscale)


important_secondary_structure_signs_dict = {}

def EnsureDir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)

def update_all_checkboxes(checkboxes, b):
    for checkbox in checkboxes:
        if b.get() ^ checkbox[1].get():
            checkbox[0].invoke()


def input_handler(root, prev_frame, pdb_id_sv, size_iv, size_min_iv, size_max_iv, overlap_iv, overlap_max_iv,
                  secondary_structure_sensitivity_bv, warning_sv):
    if pdb_id_sv.get() == '' or size_iv.get() == 0 or overlap_iv.get() == 0:
        warning_sv.set('Please fill all the mandatory (bold) fields')
    elif size_iv.get() <= overlap_iv.get():
        warning_sv.set('Please choose a smaller overlap')
    else:
        window = tk.Toplevel(root)
        window.columnconfigure(0, weight=1)
        window.rowconfigure(0, weight=1)
        results_frame = ttk.Frame(window, padding="3 3 3 3", style='My.TFrame')
        results_frame.grid(column=0, row=0, sticky=(N, E, W, S))

        results_frame.columnconfigure(0, weight=1)
        results_frame.rowconfigure(0, weight=1)

        display_frame = ttk.Frame(results_frame, padding="3 3 3 3", style='My.TFrame')
        display_frame.grid(column=0, row=0, sticky=(N, E, W, S))
        display_frame.columnconfigure(0, weight=1)
        display_frame.rowconfigure(0, weight=1)

        # Create canvas
        canvas = ResizingCanvas(display_frame)
        # Create scrollbars
        xscrollbar = Scrollbar(display_frame, orient=HORIZONTAL, command=canvas.xview)
        yscrollbar = Scrollbar(display_frame, orient=VERTICAL, command=canvas.yview )
        # Attach canvas to scrollbars
        canvas.configure(xscrollcommand=xscrollbar.set)
        canvas.configure(yscrollcommand=yscrollbar.set)

        canvas.grid(column=0, row=0)
        canvas.columnconfigure(0, weight=1)
        canvas.rowconfigure(0, weight=1)
        xscrollbar.grid(column=0, row=1, sticky="we")
        yscrollbar.grid(column=1, row=0, sticky="ns")

        canvas_id = canvas.create_text(10, 10, anchor="nw", font=('Rod', 12))
        canvas.itemconfig(canvas_id)

        important_secondary_structure_signs = [sign for sign in important_secondary_structure_signs_dict.keys() if
                                               important_secondary_structure_signs_dict[sign].get()]
        print(important_secondary_structure_signs)

        if size_min_iv.get() == 0:
            size_min_iv.set(size_iv.get())
        if size_max_iv.get() == 0:
            size_max_iv.set(size_iv.get())
        if overlap_max_iv.get() == 0:
            overlap_max_iv.set(overlap_iv.get())

        analysed_chains = evaluate(pdb_id_sv.get(), size_iv.get(), size_min_iv.get(), size_max_iv.get(),
                                   overlap_iv.get(), overlap_max_iv.get(), important_secondary_structure_signs,
                                   secondary_structure_sensitivity_bv.get())

        # put_on_canvas(analysed_chains, canvas, canvas_id)
        put_on_canvas_stacked(analysed_chains, canvas, canvas_id)

        tk.Button(results_frame, bg='#000000', fg='#b7f731', relief='flat', text='Export\nCSV', height=3, width=8,
                  command=lambda: put_in_csv(analysed_chains)).grid(column=1, row=0)

def put_on_canvas(analysed_chains, canvas, canvas_id):
    for chain_id in sorted(analysed_chains.keys()):
        peptides_offsets_letters_signs = analysed_chains[chain_id]
        max_length = len(peptides_offsets_letters_signs[0][1])
        canvas.insert(canvas_id, INSERT, 'results for chain id: {0}\n'.format(chain_id))
        canvas.insert(canvas_id, INSERT, '=' * max_length + '\n')
        for peptide_offset_letter_sign in peptides_offsets_letters_signs:
            peptide_letters = (' ' * peptide_offset_letter_sign[0]) + ''.join(peptide_offset_letter_sign[1][0])
            canvas.insert(canvas_id, INSERT, peptide_letters + '\n')
            peptide_signs = (' ' * peptide_offset_letter_sign[0]) + ''.join(peptide_offset_letter_sign[1][1])
            canvas.insert(canvas_id, INSERT, peptide_signs + '\n')
        canvas.insert(canvas_id, INSERT, '=' * max_length + '\n\n')

def put_on_canvas_stacked(analysed_chains, canvas, canvas_id):
    for chain_id in sorted(analysed_chains.keys()):
        peptides_offsets_letters_signs = analysed_chains[chain_id]
        canvas.insert(canvas_id, INSERT, 'results for chain id: {0}\n'.format(chain_id))
        chain = peptides_offsets_letters_signs[0]
        chain_letters = ''.join(chain[1][0])
        canvas.insert(canvas_id, INSERT, chain_letters + '\n')
        chain_signs = ''.join(chain[1][1])
        canvas.insert(canvas_id, INSERT, chain_signs + '\n')
        first_line_letters = ''
        first_line_signs = ''
        second_line_letters = ''
        second_line_signs = ''
        for peptide_offset_letter_sign in peptides_offsets_letters_signs[1:]:
            peptide_letters = ''.join(peptide_offset_letter_sign[1][0])
            peptide_signs = ''.join(peptide_offset_letter_sign[1][1])
            if peptides_offsets_letters_signs.index(peptide_offset_letter_sign) % 2 == 1:
                spaces = ' ' * (peptide_offset_letter_sign[0] - len(first_line_letters))
                first_line_letters = first_line_letters + spaces
                first_line_signs = first_line_signs + spaces
                first_line_letters = first_line_letters + peptide_letters
                first_line_signs = first_line_signs + peptide_signs
            else:
                spaces = ' ' * (peptide_offset_letter_sign[0] - len(second_line_letters))
                second_line_letters = second_line_letters + spaces
                second_line_signs = second_line_signs + spaces
                second_line_letters = second_line_letters + peptide_letters
                second_line_signs = second_line_signs + peptide_signs

        canvas.insert(canvas_id, INSERT, ('-' * len(chain_letters)) + '\n')
        canvas.insert(canvas_id, INSERT, first_line_letters + '\n')
        canvas.insert(canvas_id, INSERT, first_line_signs + '\n')
        canvas.insert(canvas_id, INSERT, second_line_letters + '\n')
        canvas.insert(canvas_id, INSERT, second_line_signs + '\n')
        canvas.insert(canvas_id, INSERT, ('=' * len(chain_letters)) + '\n\n')

def put_in_csv(analysed_chains):
    from tkinter import filedialog
    csv_name = filedialog.asksaveasfilename(defaultextension='csv')
    if os.path.exists(csv_name):
        os.remove(csv_name)
    with open(csv_name, 'a', encoding="utf-8") as csv:
        for chain_id in sorted(analysed_chains.keys()):
            peptides_offsets_letters_signs = analysed_chains[chain_id]
            for peptide_offset_letter_sign in peptides_offsets_letters_signs:
                peptide_letters = ''.join(peptide_offset_letter_sign[1][0])
                csv.write(peptide_letters)
                if peptides_offsets_letters_signs.index(peptide_offset_letter_sign) != len(peptides_offsets_letters_signs) - 1:
                    csv.write(',')
                else:
                    csv.write('\n')


def init(title):
    EnsureDir('PDB/')
    EnsureDir('CSV_DATA/')
    root = Tk()
    root.title(title)
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)
    s = Style()
    s.configure('My.TFrame', background='SpringGreen2')
    build_input_frame(root, None)
    root.mainloop()


def build_input_frame(root, prev_frame):
    optional_elements = []
    if prev_frame is not None:
        prev_frame.destroy()
    input_frame_width = GetSystemMetrics(0) - 50
    input_frame_height = GetSystemMetrics(1)
    father_frame = ttk.Frame(root, padding="3 3 3 3", style='My.TFrame', width=input_frame_width,
                             height=input_frame_height)
    father_frame.pack(expand=True, fill='both')
    input_frame = ttk.Frame(father_frame, padding="3 3 3 3", style='My.TFrame', width=input_frame_width,
                            height=input_frame_height)
    input_frame.pack(expand=True)
    input_frame.columnconfigure(0, weight=1)
    input_frame.rowconfigure(0, weight=1)

    # instructions and space
    ttk.Label(input_frame, text='Please fill all the relevant fields.\nBold fields are mandatory.', font=('Times', 16),
              background='SpringGreen2').grid(column=0, row=0, columnspan=10)
    ttk.Label(input_frame, text='', font=('Times', 16), background='SpringGreen2').grid(column=1, row=1)

    # fields labels
    l_pdb_id = ttk.Label(input_frame, text='pdb id', font=('Times', 12, 'bold'), background='SpringGreen2')
    l_pdb_id.grid(column=0, row=2, sticky='w')

    l_pepetide_size = ttk.Label(input_frame, text='peptide size', font=('Times', 12, 'bold'), background='SpringGreen2')
    l_pepetide_size.grid(column=0, row=3, sticky='w')

    l_pepetide_size_min = ttk.Label(input_frame, text='peptide min size', font=('Times', 12), background='SpringGreen2')
    optional_elements.append((l_pepetide_size_min, 0, 4))

    l_pepetide_size_max = ttk.Label(input_frame, text='peptide max size', font=('Times', 12), background='SpringGreen2')
    optional_elements.append((l_pepetide_size_max, 0, 5))

    l_overlap = ttk.Label(input_frame, text='overlap', font=('Times', 12, 'bold'), background='SpringGreen2')
    l_overlap.grid(column=0, row=6, sticky='w')

    l_overlap_max = ttk.Label(input_frame, text='overlap max', font=('Times', 12), background='SpringGreen2')
    optional_elements.append((l_overlap_max, 0, 7))

    # fields
    pdb_id_sv = StringVar()
    e_pdb_id = ttk.Entry(input_frame, width=6, textvariable=pdb_id_sv)
    e_pdb_id.grid(column=1, row=2)

    size_iv = IntVar()
    e_pepetide_size = ttk.Entry(input_frame, width=6, textvariable=size_iv)
    e_pepetide_size.grid(column=1, row=3)

    size_min_iv = IntVar()
    e_pepetide_size_min = ttk.Entry(input_frame, width=6, textvariable=size_min_iv)
    optional_elements.append((e_pepetide_size_min, 1, 4))

    size_max_iv = IntVar()
    e_pepetide_size_max = ttk.Entry(input_frame, width=6, textvariable=size_max_iv)
    optional_elements.append((e_pepetide_size_max, 1, 5))

    overlap_iv = IntVar()
    e_overlap = ttk.Entry(input_frame, width=6, textvariable=overlap_iv)
    e_overlap.grid(column=1, row=6)

    overlap_max_iv = IntVar()
    e_overlap_max = ttk.Entry(input_frame, width=6, textvariable=overlap_max_iv)
    optional_elements.append((e_overlap_max, 1, 7))

    def update_grid(more_options_bv, optional_elements):
        for optional_element in optional_elements:
            if more_options_bv.get():
                optional_element[0].grid(column=optional_element[1], row=optional_element[2], sticky='w')
            else:
                optional_element[0].grid_remove()

    # left checkbuttons
    more_options_bv = BooleanVar()
    c_more_options = tk.Checkbutton(input_frame, anchor="w", justify='left', activebackground='SpringGreen1',
                                    variable=more_options_bv,
                                    command=lambda: update_grid(more_options_bv, optional_elements), bg='SpringGreen2',
                                    text='More Options', font=('Times', 12))
    c_more_options.grid(column=0, row=8, sticky='w')

    secondary_structure_sensitivity_bv = BooleanVar()
    c_ss_sensitivity = tk.Checkbutton(input_frame, anchor="w", justify='left', activebackground='SpringGreen1',
                                      variable=secondary_structure_sensitivity_bv, bg='SpringGreen2',
                                      text='Secondary Structure Sensitivity', font=('Times', 12))
    c_ss_sensitivity.grid(column=0, row=9, sticky='w')

    # right checkbuttons
    secondary_structure_signs_and_meanings = [
        'G = Helix-3',
        'H = α Helix',
        'I = Helix-5',
        'T = Turn',
        'E = β Sheet',
        'B = β Bridge',
        'S = Bend'
    ]

    space = 30

    checkboxes = []
    for secondary_structure_sign_and_meaning in secondary_structure_signs_and_meanings:
        b = BooleanVar()
        sign = secondary_structure_sign_and_meaning[0]
        important_secondary_structure_signs_dict.update({sign: b})
        ttk.Label(input_frame, text=' ' * space, background='SpringGreen2').grid(sticky='w', column=2,
                                                                                 row=3 + secondary_structure_signs_and_meanings.index(
                                                                                     secondary_structure_sign_and_meaning))
        c = tk.Checkbutton(input_frame, anchor="w", justify='left', variable=b, activebackground='SpringGreen1',
                           bg='SpringGreen2', text=secondary_structure_sign_and_meaning, font=('Times', 12))
        c.grid(sticky='w', column=3,
               row=3 + secondary_structure_signs_and_meanings.index(secondary_structure_sign_and_meaning))
        checkboxes.append((c, b))

    b = BooleanVar()
    tk.Checkbutton(input_frame, anchor="w", justify='left', variable=b, activebackground='SpringGreen1',
                   command=lambda: update_all_checkboxes(checkboxes, b), bg='SpringGreen2', text='Choose all',
                   font=('Times', 12)).grid(sticky='w', column=3, row=2)
    ttk.Label(input_frame, text=' ' * space, background='SpringGreen2').grid(sticky='w', column=2, row=2)

    # warning message and execution button
    warning_sv = StringVar()
    tk.Label(input_frame, textvariable=warning_sv, font=('Times', 16), background='SpringGreen2', fg="red").grid(
        column=0, row=10, columnspan=10)
    tk.Button(input_frame, bg='#000000', fg='SpringGreen2', relief='flat', text='calculate',
              command=lambda: input_handler(root, father_frame, pdb_id_sv, size_iv, size_min_iv, size_max_iv,
                                            overlap_iv, overlap_max_iv, secondary_structure_sensitivity_bv, warning_sv),
              font=('Times', 16)).grid(column=0, row=11, columnspan=10)
    father_frame.update()


init('Peptide Calculator')
