from Bio.PDB import PDBList


def retrieve(pdb_id):
    pdbl = PDBList()
    pdbl.retrieve_pdb_file(pdb_id, pdir='PDB')
    return 'PDB\pdb{0}.ent'.format(pdb_id.lower())

