from Bio.PDB import PDBParser, DSSP
from Bio.Alphabet import IUPAC
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO

import retriever


def extract_primary_structure(PDB_filepath):
    sequences = []
    handle = open(PDB_filepath, "rU")
    for record in SeqIO.parse(handle, "pdb-seqres"):
        ## The next two lines create a sequence object/record
        ## It might be useful if you want to do something with the sequence later
        my_prot = Seq(str(''.join(record.seq)), IUPAC.protein)
        seq_obj = SeqRecord(my_prot, id=record.id, name="", description="")
        sequences.append(seq_obj)
    handle.close()
    return sequences


def extract_secondary_structure(pdb_id, pdb_file_path):
    p = PDBParser()
    s = p.get_structure(pdb_id, pdb_file_path)
    model = s[0]
    d = DSSP(model, pdb_file_path)
    secondary_sequences = {}
    for seq_id in set([key[0] for key in d.keys()]):
        secondary_sequences[seq_id] = []
    for key in d.keys():
        seq_id = key[0]
        letter = d[key][1]
        tag = d[key][2]
        secondary_sequences[seq_id].append((letter, tag))
    return secondary_sequences


def fix_secondary_sequences(sequences, secondary_sequences):
    for seq in sequences:
        id = seq.id[len(seq.id) - 1]
        seq_str = seq.seq
        sseq = secondary_sequences[id]
        fix_secondary_structure([c for c in seq_str], sseq)


def fix_secondary_structure(seq, sseq):
    i = 0
    while i < len(seq):
        if i >= len(sseq):
            sseq.append((seq[i], '?'))
        elif sseq[i][0] == 'X':
            sseq[i] = (seq[i], sseq[i][1])
        elif seq[i] != sseq[i][0]:
            sseq.insert(i, (seq[i], '?'))
        else:
            pass
        i += 1


def get_sequence(pdb_id):
    pdb_file_path = retriever.retrieve(pdb_id)
    primary_sequences = extract_primary_structure(pdb_file_path)
    secondary_sequences = extract_secondary_structure(pdb_id, pdb_file_path)
    fix_secondary_sequences(primary_sequences, secondary_sequences)
    return secondary_sequences
